В корне проекта лежит два скрипта YotaSliderStart_unix.sh и YotaSliderStart_win.bat - скрипт
с расширением .sh для систем *nix, скрипт с расширением .bat для систем windows.
В зависимости от типа вашей ОС необходимо прописать путь к исполняемому скрипту в
файле YotaSliderTests/src/main/resources/config/TestAppData.xml.
Далее открываем проект в среде и запускаем на выполнение файл
YotaSliderTests/src/main/resources/testng/YotaSliderTests.xml
