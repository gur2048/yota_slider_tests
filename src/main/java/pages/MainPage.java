package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by guska on 24.07.16.
 */
public class MainPage extends BasicPage{

    public String URL = "http://localhost:4567/index.html";

    @FindBy(xpath = "//*[@class='actions']//*[@data-bind='click: doReset']")
    public WebElement doResetButton;

    @FindBy(xpath = "//*[@class='actions']//*[@data-bind='click: doPayment']")
    public WebElement doPaymentButton;

    @FindBy(xpath = "//*[@class='tarriff-info']//*[contains(@data-bind,'doPurchase')]")
    public WebElement doPurchaseButton;

    @FindBy(xpath = "//*[@id='balance-holder']//*[contains(@data-bind,'balance')]")
    public WebElement balance;

    @FindBy(xpath = "//*[@class='form']//*[@name='amount']")
    public WebElement fieldOfPayment;

    @FindBy(xpath = "//*[@class='decrease']//*[@data-bind='click: moveLeft']")
    public WebElement sliderMoveLeft;

    @FindBy(xpath = "//*[@class='increase']//*[@data-bind='click: moveRight']")
    public WebElement sliderMoveRight;

    @FindBy(xpath = "//*[@class='cost']//*[@data-bind='text: currentCost']")
    public WebElement newServicePrice;

    @FindBy(xpath = "//*[@class='tariff']//*[@class='time']/strong")
    public WebElement newServiceDaysCount;

    @FindBy(xpath = "//*[@class='tariff']//*[@class='speed']//*[@data-bind='text: currentSpeed']")
    public WebElement newServiseSpeedConnection;

    @FindBy(xpath = "//*[@class='cost no-arrow']//*[@data-bind='text: previousCost']")
    public WebElement actualServicePrice;

    @FindBy(xpath = "//*[@class='tariff unavailable']//*[@class='time']/strong")
    public WebElement actualServiceDayCount;

    @FindBy(xpath = "//*[@class='tariff unavailable']//*[@class='speed']//*[@data-bind='text: previousSpeed']")
    public WebElement actualServiceSpeedConnection;

    public MainPage(WebDriver driver) {
        super(driver);
    }

    public <T extends BasicPage> T openPage()
    {
        driver.get(URL);
        return (T) this;
    }

    public <T extends BasicPage> T moveSliderToNewServicePrice(int ServicePrice, BasicPage page)
    {
        boolean ServicePriceIsSet = false;

        while (!ServicePriceIsSet)
        {
            int actualServicePriceAsInt = Integer.parseInt(newServicePrice.getText());

            if(actualServicePriceAsInt != ServicePrice)
            {
                if(actualServicePriceAsInt < ServicePrice)
                {
                    page.clickElementBySendKey(sliderMoveRight);
                }
                else
                {
                    page.clickElementBySendKey(sliderMoveLeft);
                }
            }
            else
            {
                ServicePriceIsSet = true;
            }
        }
        return (T) this;
    }


}
