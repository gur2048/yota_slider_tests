package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.log4testng.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by guska on 24.07.16.
 */
public abstract class BasicPage {

    public static Logger log = Logger.getLogger(BasicPage.class);
    public WebDriver driver;
    protected WebDriverWait wait;
    protected String URL;
    int count;

    public BasicPage(WebDriver driver) {
        this.driver = driver;
        count = 0;
        wait = new WebDriverWait(driver, 5);
        PageFactory.initElements(driver, this);
    }

    public boolean isElementOnPage(WebElement webElement)
    {
        try{
            wait.until(ExpectedConditions.visibilityOf(webElement));
            return true;
        }
        catch (Exception e){
            log.error("Page isn't conteins this element: " + e);
            return false;
        }
    }

    public <T extends BasicPage> T openPage()
    {
        driver.get(URL);
        return (T) this;
    }

    public <T extends BasicPage> T clickElement(WebElement webElement)
    {
        try {
            wait.until(ExpectedConditions.elementToBeClickable(webElement)).click();
            return (T) this;
        }
        catch (Exception e){
            log.error("Page isn't conteins this element: " + e);
            return null;
        }

    }

    public <T extends BasicPage> T clickElementBySendKey(WebElement webElement)
    {
        try {
            wait.until(ExpectedConditions.elementToBeClickable(webElement)).sendKeys(Keys.ENTER);
            return (T) this;
        }
        catch (Exception e){
            log.error("Page isn't conteins this element: " + e);
            return null;
        }

    }

    public <T extends BasicPage> T clickElement(String xpath)
    {
        try {
            WebElement element = driver.findElement(By.xpath(xpath));
            wait.until(ExpectedConditions.visibilityOf(element)).click();
            return (T) this;
        }
        catch (Exception e){
            log.error("Page isn't conteins this element: " + e);
            return null;
        }

    }

    public <T extends BasicPage> T typeInElement(WebElement webElement, String type)
    {
        try {
            wait.until(ExpectedConditions.visibilityOf(webElement));
            webElement.click();
            webElement.clear();
            webElement.sendKeys(type);
            Thread.sleep(2000);
            return (T) this;
        }
        catch (Exception e)
        {
            log.error("Page isn't conteins this element: " + e);
            return null;
        }
    }


    public List<WebElement> isAllElementsOnPage(List<WebElement> webElementList)
    {
        if (webElementList.size() <= count)
        {
            return  webElementList;
        }
        else if (isElementOnPage(webElementList.get(count))) {
            count++;
            isAllElementsOnPage(webElementList);
        }
        else
        {
            webElementList.remove(count);
            isAllElementsOnPage(webElementList);
        }
        return  webElementList;
    }

    public boolean isElemetClicable(WebElement webElement){
        try {
            return wait.until(ExpectedConditions.elementToBeSelected(webElement));
        }
        catch (Exception e){
            return false;
        }
    }

}
