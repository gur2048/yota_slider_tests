package utils;

/**
 * Created by guska on 24.07.16.
 */
public class TestAppUtil {

    ConfigReader configReader;
    OSUtil osUtil;

    public void startTestApp()
    {
        configReader = new ConfigReader();
        osUtil = new OSUtil();
        String scriptPath = configReader.getAppData().get("scriptPath");

        osUtil.runScript(scriptPath);
    }

}
