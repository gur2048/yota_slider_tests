package utils;

import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by guska on 24.07.16.
 */
public class ConfigReader {

    public Map<String, String> getAppData()
    {
        File file = new File("src/main/resources/config/TestAppData.xml");
        Document document = null;
        Map<String, String> getAppData = new HashMap<String, String>();
        try {
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            document = documentBuilder.parse(file);
            getAppData.put("scriptPath", document.getElementsByTagName("scriptPath").item(0).getTextContent());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return getAppData;
    }
}
