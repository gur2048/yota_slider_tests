package utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by guska on 24.07.16.
 */
public class OSUtil {


    public String executeCommand(String command)
    {
        StringBuffer output = new StringBuffer("");
        StringBuffer erroroUtput = new StringBuffer("");
        Process process;
        int count = 0;

        try
        {
            process = Runtime.getRuntime().exec(command);
            process.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));

            String line = "";
            while ((line = reader.readLine()) != null) {
                output.append(line + "\n");
                count++;
                if (count > 5)
                {
                    break;
                }
            }

            String error = "";
            while ((error = stdError.readLine()) != null) {
                erroroUtput.append(error + "\n");
                count++;
                if (count > 5)
                {
                    break;
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        if (output.length() > erroroUtput.length())
        {
            return output.toString();
        }
        else
        {
            return erroroUtput.toString();
        }

    }

    public void runScript(String scriptPath)
    {
        StringBuffer output = new StringBuffer("");
        try
        {
            Runtime.getRuntime().exec(scriptPath);
            Thread.sleep(10000);
            System.out.println("Script is started");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /*
    public static boolean isWindows()
    {
        String os = System.getProperty("os.name").toLowerCase();
        //System.out.println(os); //debug
        return (os.contains("win"));
    }

    public static boolean isUnix ()
    {
        String os = System.getProperty("os.name").toLowerCase();
        //System.out.println(os); //debug
        return (os.contains("nix") || os.contains("nux"));
    }
*/

}
