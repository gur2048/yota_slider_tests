import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by guska on 24.07.16.
 */
public class YotaRegresTests extends BasicTest {

    

    @BeforeTest
    public void beforeTest() throws InterruptedException {
        super.beforeTest();
        testAppUtil.startTestApp(); //app start
        mainPage.openPage(); //open page
        Thread.sleep(2000);
        mainPage.clickElementBySendKey(mainPage.doResetButton); //reset app
        Thread.sleep(2000);
    }

    @Test(priority = 1)
    public void test1_openMainPageTest()
    {
        softAssert = new SoftAssert();
        mainPage.openPage(); //open page
        softAssert.assertTrue(mainPage.isElementOnPage(mainPage.balance)); // balance WebElement check  on page
        softAssert.assertAll();
        log.info("test1_openMainPageTest - passed");
    }


    @Test(priority = 2)
    public void test2_addToBalance() throws InterruptedException {
        softAssert = new SoftAssert();
        int balanceSum = Integer.parseInt(mainPage.balance.getText());
        int amountOfPayment = 5000;

        //mainPage.openPage();
        mainPage.typeInElement(mainPage.fieldOfPayment, Integer.toString(amountOfPayment)) //typing money amount
                .clickElement(mainPage.doPaymentButton); // click doPaymentButton
        Thread.sleep(2000);
        int balanceSumAfterPayment = Integer.parseInt(mainPage.balance.getText()); //get balance

        softAssert.assertEquals(balanceSumAfterPayment, balanceSum + amountOfPayment);
        softAssert.assertAll();
        log.info("test2_addToBalance - passed");
    }

    @Test(priority = 3)
    public void test3_serviceActivation() throws InterruptedException {
        softAssert = new SoftAssert();
        int amountOfPayment = 5000;
        int newServicePrice = 600;

        //mainPage.openPage();
        mainPage.typeInElement(mainPage.fieldOfPayment, Integer.toString(amountOfPayment)) //typing money amount
                .clickElementBySendKey(mainPage.doPaymentButton); //click doPaymentButton
        mainPage.moveSliderToNewServicePrice(newServicePrice, mainPage) //move slider to set package of services
                .clickElementBySendKey(mainPage.doPurchaseButton); //click doPurchaseButton

        Thread.sleep(2000);
        int actualServicePrice = Integer.parseInt(mainPage.actualServicePrice.getText()); //get actual service price

        softAssert.assertEquals(actualServicePrice, newServicePrice);
        softAssert.assertAll();
        log.info("test3_serviceActivation - passed");
    }

    @Test(priority = 4)
    public void test4_PriceActivationCheсk() throws InterruptedException {
        softAssert = new SoftAssert();
        int amountOfPayment = 5000;
        int newServicePrice = 600;

        //mainPage.openPage();
        mainPage.typeInElement(mainPage.fieldOfPayment, Integer.toString(amountOfPayment)) //typing money amount
                .clickElementBySendKey(mainPage.doPaymentButton); //click doPaymentButton

        Thread.sleep(3000);
        int beforeServiceActivateBalance = Integer.parseInt(mainPage.balance.getText()); //get starting state of balance

        mainPage.moveSliderToNewServicePrice(newServicePrice, mainPage) //move slider to the chosen package of services
                .clickElementBySendKey(mainPage.doPurchaseButton); //click doPurchaseButton

        Thread.sleep(3000);
        int actualBalance = Integer.parseInt(mainPage.balance.getText()); //get actual state of balance

        softAssert.assertEquals(actualBalance, (beforeServiceActivateBalance - newServicePrice));
        softAssert.assertAll();
        log.info("test4_PriceActivationCheсk - passed");
    }

    @Test(priority = 5)
    public void test5_activationOfFreeService() throws InterruptedException {
        softAssert = new SoftAssert();
        int amountOfPayment = 300;
        int newServicePrice = 300;
        int freeServicePrice = 0;

        //mainPage.openPage();
        mainPage.typeInElement(mainPage.fieldOfPayment, Integer.toString(amountOfPayment)) //typing money amount
                .clickElementBySendKey(mainPage.doPaymentButton); //click doPaymentButton
        mainPage.moveSliderToNewServicePrice(newServicePrice, mainPage) //move slider to the chosen package of services
                .clickElementBySendKey(mainPage.doPurchaseButton); //click doPurchaseButton
        mainPage.moveSliderToNewServicePrice(freeServicePrice, mainPage) //move slider to the free package of services
                .clickElementBySendKey(mainPage.doPurchaseButton); //click doPurchaseButton

        Thread.sleep(3000);
        int actualServicePrice = Integer.parseInt(mainPage.actualServicePrice.getText()); //get actual service price

        softAssert.assertEquals(actualServicePrice, freeServicePrice);
        softAssert.assertAll();
        log.info("test5_activationOfFreeService - passed");
    }

    @Test(priority = 6)
    public void test6_sliderTest() {
        softAssert = new SoftAssert();
        int maxSliderPrice = 1400;
        int minSliderPrice = 0;
        int actualNewServicePrice;

        //mainPage.openPage();
        mainPage.moveSliderToNewServicePrice(maxSliderPrice, mainPage); //move slider to max position

        actualNewServicePrice = Integer.parseInt(mainPage.newServicePrice.getText()); //get actual price chosen service package

        softAssert.assertEquals(actualNewServicePrice, maxSliderPrice);

        mainPage.moveSliderToNewServicePrice(minSliderPrice, mainPage); //move slider to min position

        actualNewServicePrice = Integer.parseInt(mainPage.newServicePrice.getText()); //get actual price chosen service package

        softAssert.assertEquals(actualNewServicePrice, minSliderPrice);
        softAssert.assertAll();
        log.info("test6_sliderTest - passed");
    }

    @Test(priority = 7)
    public void test7_negativeValidationTest() throws InterruptedException {
        softAssert = new SoftAssert();
        int amountOfPayment = -5000;

        //mainPage.openPage();
        mainPage.typeInElement(mainPage.fieldOfPayment, Integer.toString(amountOfPayment)) //typing money amount
                .clickElementBySendKey(mainPage.doPaymentButton); //click doPaymentButton

        Thread.sleep(2000);
        int actualBalance = Integer.parseInt(mainPage.balance.getText()); //get state of balance

        softAssert.assertTrue(actualBalance >= 0);
        softAssert.assertAll();
        log.info("test7_negativeValidationTest - passed");
    }

    @Test(priority = 8)
    public void test8_banActivationWithNotEnoughBalance() throws InterruptedException {
        softAssert = new SoftAssert();
        int amountOfPayment = 300;
        int SliderPrice = 600;

        //mainPage.openPage();
        mainPage.typeInElement(mainPage.fieldOfPayment, Integer.toString(amountOfPayment)) //typing money amount
                .clickElementBySendKey(mainPage.doPaymentButton); //click doPaymentButton
        mainPage.moveSliderToNewServicePrice(SliderPrice, mainPage); //move slider to chosen not free service package
        Thread.sleep(2000);
        String webElementAttribute = mainPage.doPurchaseButton.getAttribute("class"); // get state of doPurchaseButton
        boolean isContein = webElementAttribute.contains("disabled");

        softAssert.assertTrue(isContein);
        softAssert.assertAll();
        log.info("test8_banActivationWithNotEnoughBalance - passed");
    }

    @Test(priority = 9)
    public void test9_ActivateServiceDayCounts() throws InterruptedException {
        softAssert = new SoftAssert();
        int amountOfPayment = 600;
        int SliderPrice = 600;
        int newServiseDaysCount;
        int actualServiseDaysCount;

        //mainPage.openPage();
        mainPage.typeInElement(mainPage.fieldOfPayment, Integer.toString(amountOfPayment)) //typing money amount
                .clickElementBySendKey(mainPage.doPaymentButton); //click doPaymentButton
        mainPage.moveSliderToNewServicePrice(SliderPrice, mainPage); //move slider to chosen not free service package

        Thread.sleep(2000);

        newServiseDaysCount = Integer.parseInt(mainPage.newServiceDaysCount.getText()); //get days count of chosen new service package

        mainPage.clickElementBySendKey(mainPage.doPurchaseButton); //click doPurchaseButton

        Thread.sleep(2000);

        actualServiseDaysCount = Integer.parseInt(mainPage.actualServiceDayCount.getText()); //get actual days count of active service package

        softAssert.assertEquals(actualServiseDaysCount, newServiseDaysCount);
        softAssert.assertAll();
        log.info("test9_ActivateServiseDayCounts");
    }

    @Test(priority = 10)
    public void test10_daysRecalculateWhenReactivateService() throws InterruptedException {
        softAssert = new SoftAssert();
        int amountOfPayment = 5000;
        int newServicePrice = 600;
        int nextNewServicePrice = 300;
        int secondActivationServiceDaysCount;
        int firstActivationServiseDaysCount;
        int firstServiceDaysCount;
        int secondServiceDaysCount;

        int expectedDaysCount;

        //mainPage.openPage();
        mainPage.typeInElement(mainPage.fieldOfPayment, Integer.toString(amountOfPayment)) //typing money amount
                .clickElementBySendKey(mainPage.doPaymentButton); //click doPaymentButton
        mainPage.moveSliderToNewServicePrice(newServicePrice, mainPage); //move slider to chosen not free service package
        Thread.sleep(2000);
        firstServiceDaysCount = Integer.parseInt(mainPage.newServiceDaysCount.getText()); //get days count of chosen new service package
        mainPage.clickElementBySendKey(mainPage.doPurchaseButton); //click doPurchaseButton
        mainPage.moveSliderToNewServicePrice(nextNewServicePrice, mainPage); //move slider to chosen not free service package
        Thread.sleep(2000);
        secondServiceDaysCount = Integer.parseInt(mainPage.newServiceDaysCount.getText()); //get days count of chosen new service package
        mainPage.clickElementBySendKey(mainPage.doPurchaseButton); //click doPurchaseButton
        Thread.sleep(2000);
        secondActivationServiceDaysCount = Integer.parseInt(mainPage.actualServiceDayCount.getText()); //get days count of chosen new service package

        expectedDaysCount = secondServiceDaysCount +
                        (firstServiceDaysCount *
                        (newServicePrice / nextNewServicePrice)); // calculate expected result

        softAssert.assertEquals(secondActivationServiceDaysCount, expectedDaysCount);
        softAssert.assertAll();
        log.info("test10_daysRecalculateWhenReactivateService");
    }

}
