import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.asserts.SoftAssert;
import pages.MainPage;
import utils.OSUtil;
import utils.TestAppUtil;

import java.util.concurrent.TimeUnit;

/**
 * Created by guska on 24.07.16.
 */
public class BasicTest {

    static MainPage mainPage;
    static WebDriver driver;
    static Logger log;
    static SoftAssert softAssert;
    static OSUtil osUtil;
    TestAppUtil testAppUtil;

    @BeforeTest
    public void beforeTest() throws InterruptedException {
        driver = new FirefoxDriver();
        mainPage = new MainPage(driver);
        osUtil = new OSUtil();
        testAppUtil = new TestAppUtil();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        log = Logger.getLogger(YotaRegresTests.class);
    }

    @BeforeMethod
    public void beforeMethod()
    {

    }

    @AfterMethod
    public void afterMethod()
    {
        mainPage.clickElementBySendKey(mainPage.doResetButton);
    }

    @AfterTest
    public void afterTest()
    {
        mainPage.clickElementBySendKey(mainPage.doResetButton);
        driver.quit();
    }

}
